use super::{AdjBlocks, Block, ChunkPos, LocalBlockPos};
use crate::render::{
    AttribPtrList, BlockRenderBuffer, DrawBuffer, ShaderHandle,
};
use crate::{fvec2, fvec3};
use std::cell::Ref;

const SUBCHUNK_SIZE: usize = 16 * 16 * 16;

pub struct Chunk {
    pos: ChunkPos,
    subchunks: Vec<SubChunk>,
}

pub struct AdjChunks<'a> {
    pub(super) east: Option<Ref<'a, Chunk>>,
    pub(super) west: Option<Ref<'a, Chunk>>,
    pub(super) south: Option<Ref<'a, Chunk>>,
    pub(super) north: Option<Ref<'a, Chunk>>,
}

impl Chunk {
    pub fn new(pos: ChunkPos) -> Self {
        Self {
            pos,
            subchunks: (0..16).map(|_| SubChunk::new()).collect(),
        }
    }

    pub fn set_block<P>(&mut self, pos: P, block: Block)
    where
        P: Into<LocalBlockPos>,
    {
        let pos = pos.into();
        let ref mut subchunk = self.subchunks[(pos.y / 16) as usize];
        subchunk.set_block(pos.x, pos.y.rem_euclid(16), pos.z, block);
    }

    pub fn get_block<P>(&mut self, pos: P) -> Block
    where
        P: Into<LocalBlockPos>,
    {
        let pos = pos.into();
        let ref mut subchunk = self.subchunks[(pos.y / 16) as usize];
        subchunk.get_block(pos.x, pos.y.rem_euclid(16), pos.z)
    }

    fn is_in_viewport(
        pv_mat: glm::Mat4,
        translate: glm::Vec3,
        xrng: [f32; 2],
        yrng: [f32; 2],
        zrng: [f32; 2],
    ) -> bool {
        let translate_mat = glm::translate(&glm::identity(), &translate);
        let frustum_mat = pv_mat * translate_mat;

        // Iterator over the 8 tips of the subchunk
        let points_iter = iproduct!(&xrng, &yrng, &zrng, &[1.0])
            .map(|(&x, &y, &z, &w)| frustum_mat * glm::vec4(x, y, z, w));

        points_iter.clone().any(|point| -point.w <= point.x)
            & points_iter.clone().any(|point| point.x <= point.w)
            & points_iter.clone().any(|point| -point.w <= point.y)
            & points_iter.clone().any(|point| point.y <= point.w)
            & points_iter.clone().any(|point| -point.w <= point.z)
            & points_iter.clone().any(|point| point.z <= point.w)
    }

    /// Test if subchunk at `subchunk_pos` is in the viewport after being transformed.
    /// `pv_mat` is the transformation, typically a projection matrix multiplied by a view matrix.
    fn is_subchunk_in_viewport(
        pv_mat: glm::Mat4,
        subchunk_pos: glm::Vec3,
    ) -> bool {
        Self::is_in_viewport(
            pv_mat,
            16.0 * subchunk_pos,
            [0.0, 16.0],
            [0.0, 16.0],
            [0.0, 16.0],
        )
    }

    fn is_chunk_in_viewport(pv_mat: glm::Mat4, chunk_pos: glm::Vec2) -> bool {
        let chunk_pos = glm::vec3(chunk_pos.x, 0.0, chunk_pos.y);
        Self::is_in_viewport(
            pv_mat,
            16.0 * chunk_pos,
            [0.0, 16.0],
            [0.0, 256.0],
            [0.0, 16.0],
        )
    }

    /// **Render** the chunk (i.e. to memory buffers, without involving OpenGL calls)
    pub fn render(&mut self, adj: AdjChunks, pv_mat: glm::Mat4) -> usize {
        let mut render_count = 0;

        if !Self::is_chunk_in_viewport(pv_mat, fvec2![self.pos.p, self.pos.q]) {
            return 0;
        }

        for center_index in 0..16 {
            // Skip the subchunk if it's not visible at all
            if !Self::is_subchunk_in_viewport(
                pv_mat,
                fvec3![self.pos.p, center_index, self.pos.q],
            ) {
                continue;
            }

            // Safely get mut and non-mut references
            let (bottom_subchunk, subchunk, top_subchunk) = {
                let (lo_slice, hi_slice) =
                    self.subchunks.split_at_mut(center_index);
                let (mid_slice, hi_slice) = hi_slice.split_at_mut(1);

                (
                    lo_slice.last(),
                    mid_slice.get_mut(0).unwrap(),
                    hi_slice.get(0),
                )
            };

            /// Extract reference to subchunk at `center_index` from optional `Ref` to the
            /// containing chunk
            fn get_subchunk<'a>(
                chunk: &'a Option<Ref<Chunk>>,
                center_index: usize,
            ) -> Option<&'a SubChunk> {
                chunk.as_ref().map(|chunk| &chunk.subchunks[center_index])
            };

            let adj_subchunks = AdjSubChunks {
                east: get_subchunk(&adj.east, center_index),
                west: get_subchunk(&adj.west, center_index),
                south: get_subchunk(&adj.south, center_index),
                north: get_subchunk(&adj.north, center_index),
                top: top_subchunk,
                bottom: bottom_subchunk,
            };
            render_count += subchunk.render(adj_subchunks);
        }
        render_count
    }

    /// **Draw** the chunk using OpenGL calls
    pub fn draw(&mut self, handle: ShaderHandle, pv_mat: glm::Mat4) {
        if !Self::is_chunk_in_viewport(pv_mat, fvec2![self.pos.p, self.pos.q]) {
            return;
        }
        for (subchunk, index) in self.subchunks.iter_mut().zip(0..) {
            if !Self::is_subchunk_in_viewport(
                pv_mat,
                fvec3![self.pos.p, index, self.pos.q],
            ) {
                continue;
            }

            subchunk.draw(handle, fvec3![self.pos.p, index, self.pos.q]);
        }
    }

    pub fn get_pos(&self) -> ChunkPos {
        self.pos
    }

    pub fn mark_need_render(&mut self, need_render: bool) {
        for subchunk in self.subchunks.iter_mut() {
            subchunk.need_render = need_render;
        }
    }
}

struct SubChunk {
    blocks: [Block; SUBCHUNK_SIZE],
    render_buffer: BlockRenderBuffer,
    need_render: bool,
    draw_buffer: Option<DrawBuffer>,
}

struct AdjSubChunks<'a> {
    east: Option<&'a SubChunk>,
    west: Option<&'a SubChunk>,
    south: Option<&'a SubChunk>,
    north: Option<&'a SubChunk>,
    top: Option<&'a SubChunk>,
    bottom: Option<&'a SubChunk>,
}

impl SubChunk {
    pub fn new() -> Self {
        Self {
            blocks: [Block::Air; SUBCHUNK_SIZE],
            render_buffer: BlockRenderBuffer::new(),
            need_render: true,
            draw_buffer: None,
        }
    }

    pub fn set_block(&mut self, x: u8, y: u8, z: u8, block: Block) {
        self.blocks[Self::to_index(x, y, z)] = block;
    }

    pub fn get_block(&self, x: u8, y: u8, z: u8) -> Block {
        self.blocks[Self::to_index(x, y, z)]
    }

    pub fn render(&mut self, adj: AdjSubChunks) -> usize {
        if self.need_render {
            for (x, y, z) in iproduct!(0..16, 0..16, 0..16) {
                let adj_blocks = self.get_adj_blocks(&adj, x, y, z);
                self.blocks[Self::to_index(x, y, z)].render(
                    &mut self.render_buffer,
                    LocalBlockPos { x, y, z },
                    adj_blocks,
                );
            }
            self.need_render = false;

            1
        } else {
            0
        }
    }

    fn get_adj_blocks(
        &self,
        adj: &AdjSubChunks,
        x: u8,
        y: u8,
        z: u8,
    ) -> AdjBlocks {
        AdjBlocks {
            east: if x == 15 {
                adj.east.map(|subchunk| subchunk.get_block(0, y, z))
            } else {
                Some(self.blocks[Self::to_index(x + 1, y, z)])
            },
            west: if x == 0 {
                adj.west.map(|subchunk| subchunk.get_block(15, y, z))
            } else {
                Some(self.blocks[Self::to_index(x - 1, y, z)])
            },
            top: if y == 15 {
                adj.top.map(|subchunk| subchunk.get_block(x, 0, z))
            } else {
                Some(self.blocks[Self::to_index(x, y + 1, z)])
            },
            bottom: if y == 0 {
                adj.bottom.map(|subchunk| subchunk.get_block(x, 15, z))
            } else {
                Some(self.blocks[Self::to_index(x, y - 1, z)])
            },
            south: if z == 15 {
                adj.south.map(|subchunk| subchunk.get_block(x, y, 0))
            } else {
                Some(self.blocks[Self::to_index(x, y, z + 1)])
            },
            north: if z == 0 {
                adj.north.map(|subchunk| subchunk.get_block(x, y, 15))
            } else {
                Some(self.blocks[Self::to_index(x, y, z - 1)])
            },
        }
    }

    pub fn draw(&mut self, handle: ShaderHandle, shift: glm::Vec3) {
        if self.render_buffer.is_empty() {
            return;
        }

        let ref mut render_buffer = self.render_buffer;
        let draw_buffer = match &mut self.draw_buffer {
            Some(draw_buffer) => draw_buffer,
            None => {
                // Make sure draw buffer is initialized before drawing
                self.draw_buffer = Some(DrawBuffer::new(
                    handle,
                    AttribPtrList::new()
                        .push_float(3, gl::FLOAT)
                        .push_float(2, gl::FLOAT)
                        .push_int(1, gl::UNSIGNED_BYTE),
                ));
                self.draw_buffer.as_mut().unwrap()
            }
        };

        // Setup uniforms
        handle
            .uniform_vec3("shift", shift)
            .expect("Failed to set shift vector uniform");

        // Bind draw buffer, feed data from render buffer, and then draw triangles
        draw_buffer.bind(handle, |mut handle| {
            handle.feed_render_buffer(render_buffer);
            handle.draw_triangles(gl::UNSIGNED_INT);
        });
    }

    fn to_index(x: u8, y: u8, z: u8) -> usize {
        // Extend to usize beforehand to avoid overflow
        let (x, y, z) = (x as usize, y as usize, z as usize);
        (x + z * 16 + y * 16 * 16) as usize
    }
}
