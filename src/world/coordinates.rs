#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct ChunkPos {
    pub p: i64,
    pub q: i64,
}

impl ChunkPos {
    pub fn new(p: i64, q: i64) -> Self {
        Self { p, q }
    }

    pub fn neighbor_on_dir(&self, dir: Direction) -> Self {
        use Direction::*;

        let (p, q) = (self.p, self.q);
        match dir {
            East => (p + 1, q),
            West => (p - 1, q),
            South => (p, q + 1),
            North => (p, q - 1),
        }
        .into()
    }

    pub fn neighbors(&self) -> impl Iterator<Item = ChunkPos> {
        static DIRS: [Direction; 4] = [
            Direction::East,
            Direction::West,
            Direction::South,
            Direction::North,
        ];
        let center = self.clone();
        DIRS.iter().map(move |dir| dir.as_shift() + center)
    }
}

impl From<(i64, i64)> for ChunkPos {
    fn from((p, q): (i64, i64)) -> Self {
        Self::new(p, q)
    }
}

impl std::ops::Add<ChunkPos> for ChunkPos {
    type Output = ChunkPos;
    fn add(self, rhs: ChunkPos) -> Self::Output {
        Self {
            p: self.p + rhs.p,
            q: self.q + rhs.q,
        }
    }
}

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct BlockPos {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

#[derive(Debug, Hash, Copy, Clone, Eq, PartialEq)]
pub struct LocalBlockPos {
    pub x: u8,
    pub y: u8,
    pub z: u8,
}

impl LocalBlockPos {
    pub fn new(x: u8, y: u8, z: u8) -> Self {
        Self { x, y, z }
    }
}

impl Into<LocalPos> for LocalBlockPos {
    fn into(self) -> LocalPos {
        let (x, y, z) = (self.x as f32, self.y as f32, self.z as f32);
        LocalPos { x, y, z }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct LocalPos {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl LocalPos {
    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }
}

impl Into<[f32; 3]> for LocalPos {
    fn into(self) -> [f32; 3] {
        [self.x, self.y, self.z]
    }
}

impl std::ops::Add<LocalPos> for LocalPos {
    type Output = LocalPos;
    fn add(self, rhs: LocalPos) -> Self::Output {
        LocalPos {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Direction {
    East,
    West,
    South,
    North,
}

impl Direction {
    pub fn as_shift(self) -> ChunkPos {
        match self {
            Direction::East => (1, 0),
            Direction::West => (-1, 0),
            Direction::South => (0, 1),
            Direction::North => (0, -1),
        }
        .into()
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Face {
    East,
    West,
    Top,
    Bottom,
    South,
    North,
}

impl Face {
    pub fn as_index(self) -> usize {
        match self {
            Face::East => 0,
            Face::West => 1,
            Face::Top => 2,
            Face::Bottom => 3,
            Face::South => 4,
            Face::North => 5,
        }
    }
}
