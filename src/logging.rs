use std::fs::File;
use std::io::BufWriter;

pub fn create_logfile_or_panic() -> BufWriter<File> {
    BufWriter::new(File::create("./game.log").expect("Failed to create file"))
}
