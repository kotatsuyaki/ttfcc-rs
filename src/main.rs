#[macro_use]
extern crate itertools;
extern crate nalgebra_glm as glm;
use glutin::event::Event;
use glutin::event_loop::EventLoop;
use std::io::Write;

mod render;
use render::{Shader, Texture};

mod world;
use world::ChunkRegister;

mod bindings;
use bindings::Bindings;

mod logging;

mod macros;

fn main() -> Result<(), Error> {
    let mut window = Window::new();
    window.load_gl();

    // Create shader
    let shader = Shader::new(
        include_str!("./resources/shaders/shader.vert"),
        include_str!("./resources/shaders/shader.frag"),
    )?;

    // Create chunk register
    let mut chunk_reg = ChunkRegister::new();

    // Setup main texture
    let texture = shader.bind(|handle| {
        Texture::new(
            handle,
            image::load_from_memory(include_bytes!("./resources/texture.png"))
                .expect("Failed to load image from memory"),
        )
    });

    // Setup camera
    let mut camera = render::Camera::new();

    // Create logfile
    let mut logfile = logging::create_logfile_or_panic();

    window.event_loop(
        move |window_context, elapsed, bindings, fb_size, cursor_offset| {
            /* World logic */
            let update_start = time::Instant::now();
            // Update camera before chunk I/O
            #[cfg_attr(rustfmt, rustfmt_skip)]
            {
                // Wire up key bindings with the camera
                bindings.jump
                    .run_if_pressed(|| camera.handle_fly(elapsed));
                bindings.sneak
                    .run_if_pressed(|| camera.handle_sneak(elapsed));
                bindings.left
                    .run_if_pressed(|| camera.handle_left(elapsed));
                bindings.right
                    .run_if_pressed(|| camera.handle_right(elapsed));
                bindings.forward
                    .run_if_pressed(|| camera.handle_forward(elapsed));
                bindings.backward
                    .run_if_pressed(|| camera.handle_backward(elapsed));
            }
            camera.handle_cursur_offset(cursor_offset);
            chunk_reg.perform_chunk_io(camera.get_pos());
            let _ = write!(
                logfile,
                "update,{t},",
                t = update_start.elapsed().whole_microseconds() as f64 / 1000.0
            );
            /* Render */
            let render_start = time::Instant::now();
            let mut render_count = 0;

            let proj = render::get_proj_matrix(fb_size);
            let view = camera.get_view_matrix();
            for (mut chunk, adj_chunks) in chunk_reg.adj_iter_mut() {
                render_count += chunk.render(adj_chunks, proj * view);
            }
            let _ = write!(
                logfile,
                "render,{t},",
                t = render_start.elapsed().whole_microseconds() as f64 / 1000.0
            );
            /* Draw */
            let draw_start = time::Instant::now();
            render::clear_buffer();
            shader.bind(|handle| {
                handle.set_depth_test_enabled(true);
                handle.set_cull_face_enabled(true);
                render::set_proj_and_view_uniform(
                    handle,
                    fb_size,
                    camera.get_view_matrix(),
                );
                texture.bind();

                for mut chunk in chunk_reg.iter_mut() {
                    chunk.draw(handle, proj * view);
                }
            });
            let _ = write!(
                logfile,
                "draw,{t}\n",
                t = draw_start.elapsed().whole_microseconds() as f64 / 1000.0
            );
            /* Redraw */
            window_context
                .swap_buffers()
                .expect("Failed to swap buffers");
        },
    );

    Ok(())
}

struct Window {
    event_loop: Option<EventLoop<()>>,
    window_context: Option<
        glutin::ContextWrapper<glutin::PossiblyCurrent, glutin::window::Window>,
    >,
}

impl Window {
    pub fn new() -> Self {
        let event_loop = EventLoop::new();
        let window_builder = glutin::window::WindowBuilder::new()
            .with_title("tfc-rs")
            .with_inner_size(glutin::dpi::LogicalSize::new(800, 600));
        let window_context = glutin::ContextBuilder::new()
            .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (4, 1)))
            .with_gl_profile(glutin::GlProfile::Core)
            .with_vsync(true)
            .build_windowed(window_builder, &event_loop)
            .expect("Context creation failed");
        window_context.window().set_cursor_visible(false);
        window_context
            .window()
            .set_cursor_grab(true)
            .expect("Failed to grab cursor");

        let window_context = unsafe { window_context.make_current() }
            .expect("Failed to make context current");

        Window {
            event_loop: Some(event_loop),
            window_context: Some(window_context),
        }
    }

    pub fn load_gl(&mut self) {
        gl::load_with(|s| {
            self.window_context.as_mut().unwrap().get_proc_address(s)
                as *const _
        });
        unsafe { render::setup_gl_error_callback() }
    }

    /// Event loop wrapper to hide ugly things from game loop logic.
    /// Several tasks are done here:
    ///  * closing the window on request
    ///  * Updating key binding states on keydown / keyup events
    ///  * Resize context and viewport on window resize
    ///  * Providing frame elapsed time to game loop
    pub fn event_loop<F>(mut self, mut handle_main: F)
    where
        F: 'static
            + FnMut(
                &mut glutin::ContextWrapper<
                    glutin::PossiblyCurrent,
                    glutin::window::Window,
                >,
                time::Duration,
                &mut Bindings,
                (u32, u32),
                (f32, f32),
            ),
    {
        use glutin::event_loop::ControlFlow;

        // Construct the loop context
        let mut loop_context = EventLoopContext {
            last_frame: time::Instant::now(),
            bindings: Bindings::default(),
        };

        // Tear the struct members apart
        let event_loop = self
            .event_loop
            .take()
            .expect("Failed to replace event_loop out");
        let mut window_context = self
            .window_context
            .take()
            .expect("Failed to replace window_context out");

        const PRINT_EVENTS: bool = false;

        let mut fb_size = (800, 600);
        let mut frames: usize = 0;
        let mut fps_time = time::Instant::now();
        let mut old_cursor_pos: Option<(f64, f64)> = None;
        let mut new_cursor_pos: Option<(f64, f64)> = None;

        // Main body of the loop
        event_loop.run(move |event, _what, control_flow| {
            // Closure to handle window events
            let mut handle_window_event =
                |event, control_flow: &mut ControlFlow| {
                    use glutin::event::WindowEvent::*;

                    match event {
                        // End the event loop upon window close request
                        CloseRequested => {
                            *control_flow = ControlFlow::Exit;
                            println!("Set control flow to `Exit`");
                        }
                        // Key events
                        KeyboardInput {
                            input: input_event, ..
                        } => loop_context.update_bindings(input_event),
                        // Update new cursor position (and the old one if it's None)
                        CursorMoved { position, .. } => {
                            if old_cursor_pos.is_none() {
                                old_cursor_pos = Some(position.clone().into());
                            }
                            new_cursor_pos = Some(position.into());
                        }
                        // Resize context and viewport on window resize
                        Resized(phys_size) => {
                            fb_size = (phys_size.width, phys_size.height);
                            window_context.resize(phys_size);
                            unsafe {
                                gl::Viewport(
                                    0,
                                    0,
                                    phys_size.width as i32,
                                    phys_size.height as i32,
                                )
                            }
                        }
                        _ => {}
                    }
                };

            if PRINT_EVENTS {
                println!("Event: {:?}", event);
            }

            match event {
                Event::MainEventsCleared => {
                    // Get elapsed time to pass to callback
                    let elapsed = loop_context.get_elapsed_time();

                    // FPS calculation
                    frames += 1;
                    let fps_time_elapsed = fps_time.elapsed();

                    // Print FPS every second
                    if fps_time_elapsed >= time::Duration::second() {
                        let fps: f32 =
                            frames as f32 / fps_time_elapsed.as_seconds_f32();
                        println!("FPS: {:.2}", fps);

                        // Reset timer
                        frames = 0;
                        fps_time = time::Instant::now();
                    }

                    // Calculate cursor offset
                    let cursor_offset = {
                        let old_cursor_pos =
                            old_cursor_pos.unwrap_or((0.0, 0.0));
                        let new_cursor_pos =
                            new_cursor_pos.unwrap_or((0.0, 0.0));
                        (
                            (new_cursor_pos.0 - old_cursor_pos.0) as f32,
                            (new_cursor_pos.1 - old_cursor_pos.1) as f32,
                        )
                    };
                    old_cursor_pos =
                        Some(((fb_size.0 / 2) as f64, (fb_size.1 / 2) as f64));

                    // Reset cursor to middle of the viewport
                    window_context
                        .window()
                        .set_cursor_position(
                            glutin::dpi::PhysicalPosition::new(
                                (fb_size.0 / 2) as f64,
                                (fb_size.1 / 2) as f64,
                            ),
                        )
                        .expect("Failed to reset cursor position");

                    // Call the callback
                    handle_main(
                        &mut window_context,
                        elapsed,
                        &mut loop_context.bindings,
                        fb_size,
                        cursor_offset,
                    );
                }
                Event::WindowEvent {
                    window_id: _,
                    event,
                } => {
                    handle_window_event(event, control_flow);
                }
                _ => {
                    if PRINT_EVENTS {
                        println!("Unhandled")
                    }
                }
            }
        })
    }
}

struct EventLoopContext {
    last_frame: time::Instant,
    bindings: Bindings,
}

impl EventLoopContext {
    pub fn get_elapsed_time(&self) -> time::Duration {
        self.last_frame.elapsed()
    }

    pub fn update_bindings(&mut self, event: glutin::event::KeyboardInput) {
        self.bindings.update(event);
    }
}

#[derive(Debug)]
enum Error {
    Render(render::Error),
}

impl From<render::Error> for Error {
    fn from(e: render::Error) -> Self {
        Self::Render(e)
    }
}
