[[_TOC_]]

## Notes on musl target

Rust's musl target defaults to linking statically,
which causes `winit` to fail when trying to load dynamic libraries,
preventing the window from being created.
It's been tested that dynamic target works:

```sh
RUSTFLAGS="-C target-feature=-crt-static" cargo build --release
```
